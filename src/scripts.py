from trainerbase.common.helpers import regenerate
from trainerbase.scriptengine import ScriptEngine

from objects import blood, max_blood


regeneration_script_engine = ScriptEngine(delay=0.3)


@regeneration_script_engine.simple_script
def blood_regeneration():
    regenerate(blood, max_blood, percent=1, min_value=1)
