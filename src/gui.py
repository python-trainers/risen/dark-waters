from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.scripts import ScriptUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI
from trainerbase.memory import Address

from objects import blades, blood, cunning, firearms, glory, max_blood, player_inventory_item_count, toughness, voodoo
from scripts import blood_regeneration
from teleport import tp


TAG_ITEM_OFFSET_INPUT = "tag_item_offset_input"


@simple_trainerbase_menu("Risen 2: Dark Waters", 730, 410)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(blood, "Blood", "Shift+F1", "F1", default_setter_input_value=10_000),
                GameObjectUI(max_blood, "Max Blood"),
                GameObjectUI(glory, "Glory", default_setter_input_value=10_000_000),
                SeparatorUI(),
                ScriptUI(blood_regeneration, "Blood Regeneration", "F4"),
                SeparatorUI(),
                GameObjectUI(player_inventory_item_count, "Inventory Item Count", default_setter_input_value=1_000_000),
            )

            dpg.add_input_int(
                tag=TAG_ITEM_OFFSET_INPUT,
                label="Item Offset",
                min_value=0x0,
                max_value=0x5140,
                min_clamped=True,
                max_clamped=True,
                step=0x34,
                callback=on_item_offset_change,
            )

        with dpg.tab(label="Attributes"):
            add_components(
                GameObjectUI(blades, "Blades", default_setter_input_value=10),
                GameObjectUI(firearms, "Firearms", default_setter_input_value=10),
                GameObjectUI(toughness, "Toughness", default_setter_input_value=10),
                GameObjectUI(cunning, "Cunning", default_setter_input_value=10),
                GameObjectUI(voodoo, "Voodoo", default_setter_input_value=10),
            )

        with dpg.tab(label="Teleport & Speedhack"):
            add_components(
                TeleportUI(tp),
                SeparatorUI(),
                SpeedHackUI(),
            )


def on_item_offset_change():
    assert isinstance(player_inventory_item_count.address, Address), "Item count address is not instance of Address"

    player_inventory_item_count.address.add = dpg.get_value(TAG_ITEM_OFFSET_INPUT)
