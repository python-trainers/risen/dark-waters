from trainerbase.gameobject import GameDouble, GameInt

from memory import base_address, player_base_address


player_inventory_item_count = GameInt(base_address + [0xC, 0x24, 0x28])


blood = GameInt(player_base_address + [0x14])
max_blood = GameInt(player_base_address + [0x18])
glory = GameInt(player_base_address + [0x1C])

blades = GameInt(player_base_address + [0x20])
firearms = GameInt(player_base_address + [0x24])
toughness = GameInt(player_base_address + [0x28])
cunning = GameInt(player_base_address + [0x2C])
voodoo = GameInt(player_base_address + [0x30])

player_coords_address = base_address + [0x28, 0x38, 0xC, 0x4, 0x118]
player_x = GameDouble(player_coords_address + 0x0)
player_z = GameDouble(player_coords_address + 0x8)
player_y = GameDouble(player_coords_address + 0x10)
